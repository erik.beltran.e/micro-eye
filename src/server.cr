require "kemal"
require "json"
# bind = ""
# port = 8080#
##
# OptionParser.parse! do |parser|
#  parser.banner = " Only a micro server! "
#  parser.on("-port PORT", "--port=PORT", "port") { |aport| port = aport.to_i }
#  `.on("-b BIND", "--bind=BIND", "bind") { |abind| bind = abind }
#  parser.on("-h", "--help", "Show this help") { puts parser }
# end
# =end

# Matches GET "http://host:port/"
post "/receive" do |env|
  #  p env.params.body["info"]?
  #  p env.params.body
  body = env.request.body.as(IO).gets_to_end
  parsed = JSON.parse(body) rescue nil
  if parsed.nil?
    "fail"
  else
    key = parsed["key"]
    info = parsed["info"]
    puts "key", key
    puts "Hostname", info["hostname"]
    puts "DiskInfo", info["disks"]
    "ok"
  end
end

Kemal.run
