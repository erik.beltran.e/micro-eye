require "json"
require "option_parser"
require "http/client"

# df -P / | awk '/%/ {print 0 -$}'
def cmd(bcmd)
  cmd = "sh"
  args = [] of String
  args << "-c" << bcmd
  output = IO::Memory.new
  Process.run(cmd, args, output: output)
  output.close
  return output.to_s
end

key = ""
server = ""
exclude = ""

OptionParser.parse! do |parser|
  parser.banner = " Only a micro client! "
  parser.on("-s IP:PORT", "--server=IP:PORT", "Server") { |aserver| server = aserver }
  parser.on("-k KEY", "--key=KEY", "Key") { |akey| key = akey }
  parser.on("-h", "--help", "Show this help") { puts parser }
end

# output = cmd("grep 'cpu ' /proc/stat | awk '{usage=($2+$4)*100/($2+$4+$5)} END {print usage \" \"}'")
numcpu = cmd("grep -c ^processor /proc/cpuinfo").to_i
loadavg = cmd("uptime").split("average: ")[1].strip("\n")
loadcalc = loadavg.split(", ")[0].gsub(",", ".").to_f
cpu_calc = (loadcalc / numcpu) * 100
if cpu_calc > 99
  cpu_calc = 100
end
cpu = {"cores" => numcpu, "loadavg" => loadavg, "usage" => cpu_calc}

output = cmd("df -h").split("\n")
disks = [] of Hash(String, String)
output.each do |lines|
  if lines[0..4] == "/dev/"
    infodisk = lines.gsub(/\s+/m, ' ').strip.split(" ")
    disks << {"path" => infodisk[0], "size" => infodisk[1], "used" => infodisk[2], "available" => infodisk[3], "usage_percent" => infodisk[4].gsub("%", "")}
  end
end

output = cmd("free").split("\n")
mems = [] of Hash(String, String)
output.each do |lines|
  if lines[0..2].downcase == "mem"
    infoline = lines.gsub(/\s+/m, ' ').strip.split(" ")
    calc = (infoline[2].to_f / infoline[1].to_f) * 100
    mems << {"type" => "mem", "size" => infoline[1], "used" => infoline[2], "free" => infoline[3], "available" => infoline[4], "usage_percent" => calc.to_f.to_i.to_s}
  end
  if lines[0..2].downcase == "swa"
    infoline = lines.gsub(/\s+/m, ' ').strip.split(" ")
    calc = (infoline[2].to_f / infoline[1].to_f) * 100
    mems << {"type" => "swap", "size" => infoline[1], "used" => infoline[2], "free" => infoline[3], "usage_percent" => calc.to_f.to_i.to_s}
  end
end

info = {"hostname" => System.hostname, "disks" => disks, "mem" => mems, "cpu_percentage" => cpu}

if !(key == "" || server == "")
  puts "Reporting to: http://#{server} - #{key}"
  infos = server.split(":")
  port = infos[1]
  server = infos[0]
  client = HTTP::Client.new server, port
  response = client.post("/receive", body: {"info" => info, "key" => key}.to_json.to_s)
  if response.body != "ok"
    puts "Comunication Error!"
  else
    puts "Comunication done!"
  end
end

puts info.to_json
